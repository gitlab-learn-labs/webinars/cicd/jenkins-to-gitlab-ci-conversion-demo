package hello;

import java.util.Random;

public class HelloWorld {
	public static void main(String[] args) {
		Greeter greeter = new Greeter();
		System.out.println(greeter.sayHello());
	}

	// Inject a vulnerability on purpose
	//public String generateSecretToken() {
    //    Random r = new Random();
    //    return Long.toHexString(r.nextLong());
    //}

}
